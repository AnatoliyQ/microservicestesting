package factory;

public class Constants {

    public static final String URI = "https://brains.tassta.com";
    public static final int PORT = 4000;
    public static final String USERNAME = "demo70";
    public static final String USERPASSWORD = "demo";
    public static final int USER_ID = 70;
    public static final int SERVER_ID = 2;

    // Authorizations path :
    public static final String SESSIONS = "/api/sessions";

    //AMGW RPC path
    public static final String AMGW = "/rpc";

    //AMGW RPC path
    // ---------------- SERVER ---------------------
    public static final String CREATE_SERVER_METHOD = "Server.Create";
    public static final String READ_SERVER_METHOD = "Server.Read";
    public static final String UPDATE_SERVER_METHOD = "Server.Update";
    public static final String START_SERVER_METHOD = "Server.Start";
    public static final String STOP_SERVER_METHOD = "Server.Stop";
    public static final String DELETE_SERVER_METHOD = "Server.Delete";

    // ---------------- CHANNEL ---------------------
    public static final String CREATE_CHANNEL_METHOD = "Channel.Create";
    public static final String READ_CHANNEL_METHOD = "Channel.Read";
    public static final String UPDATE_CHANNEL_METHOD = "Channel.Update";
    public static final String DELETE_CHANNEL_METHOD = "Channel.Delete";

    // ---------------- USER ---------------------
    public static final String CREATE_USER_METHOD = "User.Create";
    public static final String READ_USER_METHOD = "User.Read";
    public static final String UPDATE_USER_METHOD = "User.Update";
    public static final String DELETE_USER_METHOD = "User.Delete";

    // ---------------- TEAM ---------------------
    public static final String CREATE_TEAM_METHOD = "Team.Create";
    public static final String READ_TEAM_METHOD = "Team.Read";
    public static final String DELETE_TEAM_METHOD = "Team.Delete";

    // ---------------- Rule ---------------------
    public static final String CREATE_RULE_METHOD = "Rule.Create";
    public static final String READ_RULE_METHOD = "Rule.Read";
    public static final String UPDATE_RULE_METHOD = "Rule.Update";
    public static final String DELETE_RULE_METHOD = "Rule.Delete";

    // LWP server
    public static final String LONE_WORKERS = "/api/lone_workers";


    //ConfigServer
    public static final String AMGW_TOKEN = "/api/config/amgw/service_key";

    //Config API
    public static final String PING_PERIOD = "/api/config/general/ping_period";

    //model.connection actions
    public static final String PING = "/actions/model.connection/ping";
    public static final String DISCONNECT = "/actions/model.connection/disconnect";
    public static final String CONNECTEDUSERS = "/actions/model.connection/connected_users";

    //SSE
    public static final String SUBSCRIBE = "/actions/events/subscribe/sse";
    public static final String SSEPONG = "/actions/events/sse/pong";


}
