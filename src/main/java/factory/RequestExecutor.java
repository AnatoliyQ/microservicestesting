package factory;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.ser.FilterProvider;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.ResponseEntity;
import org.springframework.web.client.RestTemplate;
import util.Setup;

public class RequestExecutor {

    public static <T> ResponseEntity<T> doRequest(String url, HttpMethod httpMethod, Object body, Class<T> className) {
        RestTemplate restTemplate = Setup.setupRestTemplate();

        HttpHeaders headers = new HttpHeaders();
        headers.add("Content-Type", "application/json");

        ObjectMapper mapper = new ObjectMapper();

        String bodyString = null;
        try {
            bodyString = mapper.writeValueAsString(body);
        } catch (JsonProcessingException e) {
            e.printStackTrace();
        }

        HttpEntity<String> entity = new HttpEntity<>(bodyString, headers);

        return restTemplate.exchange(url, httpMethod, entity, className);
    }

    public static <T> ResponseEntity<T> doRequest(String url, HttpMethod httpMethod, String token, Class<T> className) {
        RestTemplate restTemplate = Setup.setupRestTemplate();

        HttpHeaders headers = new HttpHeaders();
        headers.add("Content-Type", "application/json");
        headers.add("Authorization", token);

        HttpEntity<String> entity = new HttpEntity<>(headers);

        return restTemplate.exchange(url, httpMethod, entity, className);
    }

    public static <T> ResponseEntity<T> doRequest(String url, HttpMethod httpMethod, String token, Object body, Class<T> className) {
        RestTemplate restTemplate = Setup.setupRestTemplate();

        HttpHeaders headers = new HttpHeaders();
        headers.add("Content-Type", "application/json");
        headers.add("Authorization", token);

        ObjectMapper mapper = new ObjectMapper();

        String bodyString = null;
        try {
            bodyString = mapper.writeValueAsString(body);
        } catch (JsonProcessingException e) {
            e.printStackTrace();
        }

//        System.out.println("Boyd without filter " + bodyString);

        HttpEntity<String> entity = new HttpEntity<>(bodyString, headers);

        return restTemplate.exchange(url, httpMethod, entity, className);
    }

    public static <T> ResponseEntity<T> doFilteredRequest(String url, HttpMethod httpMethod, String token, Object body, FilterProvider filter, Class<T> className) {
        RestTemplate restTemplate = Setup.setupRestTemplate();

        HttpHeaders headers = new HttpHeaders();
        headers.add("Content-Type", "application/json");
        headers.add("Authorization", token);

        ObjectMapper mapper = new ObjectMapper();

        String bodyString = null;
        try {
            bodyString = mapper.writer(filter).writeValueAsString(body);
        } catch (JsonProcessingException e) {
            e.printStackTrace();
        }

//        System.out.println("Update by filter " + bodyString);

        HttpEntity<String> entity = new HttpEntity<>(bodyString, headers);

        return restTemplate.exchange(url, httpMethod, entity, className);
    }
}
