package model;

import com.fasterxml.jackson.annotation.JsonProperty;

public class Response {
    @JsonProperty("message")
    private String message;

    @JsonProperty("success")
    private Boolean success;

    public String getMessage() {
        return message;
    }

    public Boolean isSuccess() {
        return success;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public void setSuccess(Boolean success) {
        this.success = success;
    }


}
