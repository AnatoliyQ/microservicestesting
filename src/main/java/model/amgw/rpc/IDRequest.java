package model.amgw.rpc;

import com.fasterxml.jackson.annotation.JsonProperty;

public class IDRequest {
    @JsonProperty("id")
    private int id;

    public void setId(int id) {
        this.id = id;
    }

    public int getId() {

        return id;
    }
}
