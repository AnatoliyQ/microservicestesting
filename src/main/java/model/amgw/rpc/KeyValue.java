package model.amgw.rpc;

import com.fasterxml.jackson.annotation.JsonProperty;

public class KeyValue {
    @JsonProperty("key")
    private String key;

    @JsonProperty("value")
    private String value;

    public KeyValue(String key, String value) {
        this.key = key;
        this.value = value;
    }

    public KeyValue() {
    }

    public String getKey() {
        return key;
    }

    public String getValue() {
        return value;
    }

    public void setKey(String key) {
        this.key = key;
    }

    public void setValue(String value) {
        this.value = value;
    }
}
