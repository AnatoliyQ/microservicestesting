package model.amgw.rpc;

import com.fasterxml.jackson.annotation.JsonProperty;

public class ObjectRpcResponse extends RpcResponse {
    @JsonProperty("result")
    private Object result;

    public Object getResult() {
        return result;
    }

    public void setResult(Object params) {
        this.result = params;
    }
}
