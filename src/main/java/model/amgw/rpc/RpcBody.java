package model.amgw.rpc;

import com.fasterxml.jackson.annotation.JsonProperty;

public class RpcBody {
    @JsonProperty("method")
    private String method;

    @JsonProperty("params")
    private Object[] params;

    public String getMethod() {
        return method;
    }

    public Object[] getParams() {
        return params;
    }

    public void setMethod(String method) {
        this.method = method;
    }

    public void setParams(Object[] params) {
        this.params = params;
    }
}
