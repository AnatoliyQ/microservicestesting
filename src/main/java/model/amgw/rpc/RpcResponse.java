package model.amgw.rpc;

import com.fasterxml.jackson.annotation.JsonProperty;

public class RpcResponse {
    @JsonProperty("id")
    private int id;


    @JsonProperty("error")
    private String error;

    public int getId() {
        return id;
    }

    public String getError() {
        return error;
    }

    public void setId(int id) {
        this.id = id;
    }


    public void setError(String error) {
        this.error = error;
    }
}

