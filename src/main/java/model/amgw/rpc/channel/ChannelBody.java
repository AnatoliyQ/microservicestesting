package model.amgw.rpc.channel;

import com.fasterxml.jackson.annotation.JsonFilter;
import com.fasterxml.jackson.annotation.JsonProperty;
import model.amgw.rpc.KeyValue;

@JsonFilter("FilterChannelBody")
public class ChannelBody {
    @JsonProperty("id")
    private int id;

    @JsonProperty("server_id")
    private int server_id;

    @JsonProperty("name")
    private String name;

    @JsonProperty("config")
    private KeyValue[] config;

    public ChannelBody() {

    }

    public int getId() {
        return id;
    }

    public int getServer_id() {
        return server_id;
    }

    public String getName() {
        return name;
    }

    public KeyValue[] getConfig() {
        return config;
    }

    public void setId(int id) {
        this.id = id;
    }

    public void setServer_id(int server_id) {
        this.server_id = server_id;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setConfig(KeyValue[] config) {
        this.config = config;
    }
}
