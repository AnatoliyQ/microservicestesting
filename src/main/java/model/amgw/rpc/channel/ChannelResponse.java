package model.amgw.rpc.channel;

import com.fasterxml.jackson.annotation.JsonProperty;
import model.amgw.rpc.RpcResponse;


public class ChannelResponse extends RpcResponse {
    @JsonProperty("result")
    private ChannelBody result;

    public ChannelBody getResult() {
        return result;
    }

    public void setResult(ChannelBody params) {
        this.result = params;
    }
}
