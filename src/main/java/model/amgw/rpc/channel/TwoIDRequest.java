package model.amgw.rpc.channel;


import com.fasterxml.jackson.annotation.JsonProperty;
import model.amgw.rpc.IDRequest;

public class TwoIDRequest extends IDRequest {
    private int server_id;

    public void setServerId(int server_id) {
        this.server_id = server_id;
    }

    @JsonProperty("server_id")
    public int getServerId() {
        return server_id;
    }

    @Override
    public String toString() {
        return super.toString() + "server_id - " + server_id;
    }
}
