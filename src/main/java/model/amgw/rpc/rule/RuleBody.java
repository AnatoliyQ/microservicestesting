package model.amgw.rpc.rule;

import com.fasterxml.jackson.annotation.JsonFilter;
import com.fasterxml.jackson.annotation.JsonProperty;

@JsonFilter("FilterRuleBody")
public class RuleBody {
    @JsonProperty("channel_id")
    private int channel_id;

    @JsonProperty("server_id")
    private int server_id;

    @JsonProperty("priority")
    private int priority;

    @JsonProperty("team_name")
    private String team_name;

    @JsonProperty("rule_apply")
    String[] rule_apply;

    @JsonProperty("rule_deny")
    String[] rule_deny;

    public int getChannel_id() {
        return channel_id;
    }

    public int getServer_id() {
        return server_id;
    }

    public int getPriority() {
        return priority;
    }

    public String getTeam_name() {
        return team_name;
    }

    public String[] getRule_apply() {
        return rule_apply;
    }

    public String[] getRule_deny() {
        return rule_deny;
    }

    public void setChannel_id(int channel_id) {
        this.channel_id = channel_id;
    }

    public void setServer_id(int server_id) {
        this.server_id = server_id;
    }

    public void setPriority(int priority) {
        this.priority = priority;
    }

    public void setTeam_name(String team_name) {
        this.team_name = team_name;
    }

    public void setRule_apply(String[] rule_apply) {
        this.rule_apply = rule_apply;
    }

    public void setRule_deny(String[] rule_deny) {
        this.rule_deny = rule_deny;
    }
}
