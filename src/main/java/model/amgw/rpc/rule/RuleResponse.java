package model.amgw.rpc.rule;

import com.fasterxml.jackson.annotation.JsonProperty;
import model.amgw.rpc.RpcResponse;

public class RuleResponse extends RpcResponse {
    @JsonProperty("result")
    private RuleBody result;

    public RuleBody getResult() {
        return result;
    }

    public void setResult(RuleBody params) {
        this.result = params;
    }
}
