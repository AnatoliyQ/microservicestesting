package model.amgw.rpc.server;

import com.fasterxml.jackson.annotation.JsonFilter;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import model.amgw.rpc.KeyValue;


@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonFilter("FilterServerBody")
public class ServerBody {
    @JsonProperty("id")
    private int id;

    @JsonProperty("port")
    private int port;

    @JsonProperty("running")
    private boolean running;

    @JsonProperty("online_users")
    private int online_users;

    @JsonProperty("name_alias")
    private String name_alias;

    @JsonProperty("config")
    private KeyValue[] config = null;

    public ServerBody() {

    }

    public String getName_alias() {
        return name_alias;
    }

    public KeyValue[] getConfig() {
        return config;
    }

    public void setName_alias(String name_alias) {
        this.name_alias = name_alias;
    }

    public void setConfig(KeyValue[] config) {
        this.config = config;
    }

    public int getId() {
        return id;
    }

    public int getPort() {
        return port;
    }

    public boolean isRunning() {
        return running;
    }

    public int getOnline_users() {
        return online_users;
    }

    public void setId(int id) {
        this.id = id;
    }

    public void setPort(int port) {
        this.port = port;
    }

    public void setRunning(boolean running) {
        this.running = running;
    }

    public void setOnline_users(int online_users) {
        this.online_users = online_users;
    }
}
