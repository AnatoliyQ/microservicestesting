package model.amgw.rpc.server;

import com.fasterxml.jackson.annotation.JsonProperty;
import model.amgw.rpc.RpcResponse;


public class ServerResponse extends RpcResponse {
    @JsonProperty("result")
    private ServerBody result;

    public ServerBody getResult() {
        return result;
    }

    public void setResult(ServerBody params) {
        this.result = params;
    }
}


