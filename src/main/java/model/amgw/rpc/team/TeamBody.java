package model.amgw.rpc.team;

import com.fasterxml.jackson.annotation.JsonFilter;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonFilter("FilterTeamBody")
public class TeamBody {
    @JsonProperty("id")
    private int id;

    @JsonProperty("server_id")
    private int server_id;

    @JsonProperty("name")
    private String name;

    @JsonProperty("users")
    private int[] users;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getServer_id() {
        return server_id;
    }

    public String getName() {
        return name;
    }

    public int[] getUsers() {
        return users;
    }

    public void setServer_id(int server_id) {
        this.server_id = server_id;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setUsers(int[] users) {
        this.users = users;
    }
}
