package model.amgw.rpc.team;

import com.fasterxml.jackson.annotation.JsonProperty;
import model.amgw.rpc.RpcResponse;


public class TeamResponse extends RpcResponse {
    @JsonProperty("result")
    private TeamBody result;

    public TeamBody getResult() {
        return result;
    }

    public void setResult(TeamBody params) {
        this.result = params;
    }
}


