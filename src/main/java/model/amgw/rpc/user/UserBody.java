package model.amgw.rpc.user;

import com.fasterxml.jackson.annotation.JsonFilter;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import model.amgw.rpc.KeyValue;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonFilter("FilterUserBody")
public class UserBody {
    @JsonProperty("id")
    private int id;

    @JsonProperty("server_id")
    private int server_id;

    @JsonProperty("name")
    private String name;

    @JsonProperty("password")
    private String password;

    @JsonProperty("last_activity")
    private int last_activity;

    @JsonProperty("config")
    private KeyValue[] config = null;

    public int getServer_id() {
        return server_id;
    }

    public String getName() {
        return name;
    }

    public String getPassword() {
        return password;
    }

    public int getLast_activity() {
        return last_activity;
    }

    public KeyValue[] getConfig() {
        return config;
    }

    public void setServer_id(int server_id) {
        this.server_id = server_id;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public void setLast_activity(int last_activity) {
        this.last_activity = last_activity;
    }

    public void setConfig(KeyValue[] config) {
        this.config = config;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }
}
