package model.amgw.rpc.user;

import com.fasterxml.jackson.annotation.JsonProperty;
import model.amgw.rpc.RpcResponse;


public class UserResponse extends RpcResponse {
    @JsonProperty("result")
    private UserBody result;

    public UserBody getResult() {
        return result;
    }

    public void setResult(UserBody params) {
        this.result = params;
    }
}
