package model.autorization;


import com.fasterxml.jackson.annotation.JsonProperty;

import java.util.Arrays;


public class AuthUserData {

    @JsonProperty("id")
    private int id;

    @JsonProperty("server_id")
    private int server_id;

    @JsonProperty("password")
    private String password;

    @JsonProperty("roles")
    private String[] roles;

    @JsonProperty("service_name")
    private String service_name;


    public int getId() {
        return id;
    }

    public int getServer_id() {
        return server_id;
    }

    public String getPassword() {
        return password;
    }

    public String[] getRoles() {
        return roles;
    }

    public String getService_name() {
        return service_name;
    }

    public void setId(int id) {
        this.id = id;

    }

    public void setServer_id(int server_id) {
        this.server_id = server_id;

    }

    public void setPassword(String password) {
        this.password = password;
    }

    public void setRoles(String[] roles) {
        this.roles = roles;
    }

    public void setService_name(String service_name) {
        this.service_name = service_name;
    }

    @Override
    public String toString() {
        return "AuthUserData{" +
                "id=" + id +
                ", server_id=" + server_id +
                ", password='" + password + '\'' +
                ", roles=" + Arrays.toString(roles) +
                ", service_name='" + service_name + '\'' +
                '}';
    }
}
