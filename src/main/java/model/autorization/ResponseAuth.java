package model.autorization;


import com.fasterxml.jackson.annotation.JsonProperty;
import model.Response;


public class ResponseAuth extends Response {
    @JsonProperty("token")
    private String token;

    public String getToken() {
        return token;
    }

    public void setToken(String token) {
        this.token = token;
    }

}
