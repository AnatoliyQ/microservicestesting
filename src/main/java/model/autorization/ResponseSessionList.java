package model.autorization;

import model.Response;

public class ResponseSessionList extends Response {

    private AuthUserData user;

    public AuthUserData getUser() {
        return user;
    }

    public void setUser(AuthUserData user) {
        this.user = user;
    }

}
