package model.configService;

import com.fasterxml.jackson.annotation.JsonProperty;
import model.Response;

public class configResponse extends Response {
    @JsonProperty
    private String value;

    public void setValue(String value) {
        this.value = value;
    }

    public String getValue() {
        return value;
    }
}
