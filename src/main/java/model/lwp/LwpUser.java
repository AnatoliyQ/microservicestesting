package model.lwp;

import com.fasterxml.jackson.annotation.JsonFilter;
import com.fasterxml.jackson.annotation.JsonProperty;

@JsonFilter("FilterLwpUserBody")
public class LwpUser {
    @JsonProperty("id")
    private int id;

    @JsonProperty("created_at")
    private String created_at;

    @JsonProperty("updated_at")
    private String updated_at;

    @JsonProperty("user_id")
    private int user_id;

    @JsonProperty("server_id")
    private int server_id;

    @JsonProperty("last_sensor_check")
    private String last_sensor_check;

    @JsonProperty("sensor_check_error")
    private String sensor_check_error;

    @JsonProperty("sc_panic_gsm")
    private boolean sc_panic_gsm;

    @JsonProperty("sc_gps")
    private boolean sc_gps;

    @JsonProperty("enabled")
    private boolean enabled;

    @JsonProperty("panic_call_enabled")
    private boolean panic_call_enabled;

    @JsonProperty("panic_sms_enabled")
    private boolean panic_sms_enabled;

    @JsonProperty("panic_email_enabled")
    private boolean panic_email_enabled;

    @JsonProperty("emergency_sms_phone")
    private String emergency_sms_phone;

    @JsonProperty("emergency_email")
    private String emergency_email;

    @JsonProperty("emergency_phone")
    private String emergency_phone;

    @JsonProperty("warning_timer")
    private int warning_timer;

    @JsonProperty("periodic_check")
    private boolean periodic_check;

    @JsonProperty("periodic_alarm_duration")
    private int periodic_alarm_duration;

    @JsonProperty("man_down")
    private boolean man_down;

    @JsonProperty("nfc_mandown")
    private boolean nfc_mandown;

    @JsonProperty("tilt_timer")
    private int tilt_timer;

    @JsonProperty("degree")
    private int degree;

    @JsonProperty("track_movement")
    private boolean track_movement;

    @JsonProperty("inactivity_timer")
    private int inactivity_timer;

    @JsonProperty("battery_emergency_call_enabled")
    private boolean battery_emergency_call_enabled;

    @JsonProperty("battery_level_monitor")
    private boolean battery_level_monitor;

    @JsonProperty("battery_level_alarm")
    private int battery_level_alarm;

    @JsonProperty("battery_level_warning")
    private int battery_level_warning;

    @JsonProperty("disconnect_monitor")
    private boolean disconnect_monitor;

    @JsonProperty("connect_monitor")
    private boolean connect_monitor;

    @JsonProperty("yellow_alarm_check_time")
    private int yellow_alarm_check_time;

    @JsonProperty("fall_detection_enabled")
    private boolean fall_detection_enabled;

    @JsonProperty("fall_threshold")
    private double fall_threshold;

    @JsonProperty("free_fall_time")
    private int free_fall_time;

    @JsonProperty("disable_on_charging")
    private boolean disable_on_charging;

    public int getId() {
        return id;
    }

    public String getCreated_at() {
        return created_at;
    }

    public String getUpdated_at() {
        return updated_at;
    }

    public int getUser_id() {
        return user_id;
    }

    public int getServer_id() {
        return server_id;
    }

    public String getLast_sensor_check() {
        return last_sensor_check;
    }

    public String getSensor_check_error() {
        return sensor_check_error;
    }

    public boolean isSc_panic_gsm() {
        return sc_panic_gsm;
    }

    public boolean isSc_gps() {
        return sc_gps;
    }

    public boolean isEnabled() {
        return enabled;
    }

    public boolean isPanic_call_enabled() {
        return panic_call_enabled;
    }

    public boolean isPanic_sms_enabled() {
        return panic_sms_enabled;
    }

    public boolean isPanic_email_enabled() {
        return panic_email_enabled;
    }

    public String getEmergency_sms_phone() {
        return emergency_sms_phone;
    }

    public String getEmergency_email() {
        return emergency_email;
    }

    public String getEmergency_phone() {
        return emergency_phone;
    }

    public int getWarning_timer() {
        return warning_timer;
    }

    public boolean isPeriodic_check() {
        return periodic_check;
    }

    public int getPeriodic_alarm_duration() {
        return periodic_alarm_duration;
    }

    public boolean isMan_down() {
        return man_down;
    }

    public boolean isNfc_mandown() {
        return nfc_mandown;
    }

    public int getTilt_timer() {
        return tilt_timer;
    }

    public int getDegree() {
        return degree;
    }

    public boolean isTrack_movement() {
        return track_movement;
    }

    public int getInactivity_timer() {
        return inactivity_timer;
    }

    public boolean isBattery_emergency_call_enabled() {
        return battery_emergency_call_enabled;
    }

    public boolean isBattery_level_monitor() {
        return battery_level_monitor;
    }

    public int getBattery_level_alarm() {
        return battery_level_alarm;
    }

    public int getBattery_level_warning() {
        return battery_level_warning;
    }

    public boolean isDisconnect_monitor() {
        return disconnect_monitor;
    }

    public boolean isConnect_monitor() {
        return connect_monitor;
    }

    public int getYellow_alarm_check_time() {
        return yellow_alarm_check_time;
    }

    public boolean isFall_detection_enabled() {
        return fall_detection_enabled;
    }

    public double getFall_threshold() {
        return fall_threshold;
    }

    public int getFree_fall_time() {
        return free_fall_time;
    }

    public boolean isDisable_on_charging() {
        return disable_on_charging;
    }

    public void setId(int id) {
        this.id = id;
    }

    public void setCreated_at(String created_at) {
        this.created_at = created_at;
    }

    public void setUpdated_at(String updated_at) {
        this.updated_at = updated_at;
    }

    public void setUser_id(int user_id) {
        this.user_id = user_id;
    }

    public void setServer_id(int server_id) {
        this.server_id = server_id;
    }

    public void setLast_sensor_check(String last_sensor_check) {
        this.last_sensor_check = last_sensor_check;
    }

    public void setSensor_check_error(String sensor_check_error) {
        this.sensor_check_error = sensor_check_error;
    }

    public void setSc_panic_gsm(boolean sc_panic_gsm) {
        this.sc_panic_gsm = sc_panic_gsm;
    }

    public void setSc_gps(boolean sc_gps) {
        this.sc_gps = sc_gps;
    }

    public void setEnabled(boolean enabled) {
        this.enabled = enabled;
    }

    public void setPanic_call_enabled(boolean panic_call_enabled) {
        this.panic_call_enabled = panic_call_enabled;
    }

    public void setPanic_sms_enabled(boolean panic_sms_enabled) {
        this.panic_sms_enabled = panic_sms_enabled;
    }

    public void setPanic_email_enabled(boolean panic_email_enabled) {
        this.panic_email_enabled = panic_email_enabled;
    }

    public void setEmergency_sms_phone(String emergency_sms_phone) {
        this.emergency_sms_phone = emergency_sms_phone;
    }

    public void setEmergency_email(String emergency_email) {
        this.emergency_email = emergency_email;
    }

    public void setEmergency_phone(String emergency_phone) {
        this.emergency_phone = emergency_phone;
    }

    public void setWarning_timer(int warning_timer) {
        this.warning_timer = warning_timer;
    }

    public void setPeriodic_check(boolean periodic_check) {
        this.periodic_check = periodic_check;
    }

    public void setPeriodic_alarm_duration(int periodic_alarm_duration) {
        this.periodic_alarm_duration = periodic_alarm_duration;
    }

    public void setMan_down(boolean man_down) {
        this.man_down = man_down;
    }

    public void setNfc_mandown(boolean nfc_mandown) {
        this.nfc_mandown = nfc_mandown;
    }

    public void setTilt_timer(int tilt_timer) {
        this.tilt_timer = tilt_timer;
    }

    public void setDegree(int degree) {
        this.degree = degree;
    }

    public void setTrack_movement(boolean track_movement) {
        this.track_movement = track_movement;
    }

    public void setInactivity_timer(int inactivity_timer) {
        this.inactivity_timer = inactivity_timer;
    }

    public void setBattery_emergency_call_enabled(boolean battery_emergency_call_enabled) {
        this.battery_emergency_call_enabled = battery_emergency_call_enabled;
    }

    public void setBattery_level_monitor(boolean battery_level_monitor) {
        this.battery_level_monitor = battery_level_monitor;
    }

    public void setBattery_level_alarm(int battery_level_alarm) {
        this.battery_level_alarm = battery_level_alarm;
    }

    public void setBattery_level_warning(int battery_level_warning) {
        this.battery_level_warning = battery_level_warning;
    }

    public void setDisconnect_monitor(boolean disconnect_monitor) {
        this.disconnect_monitor = disconnect_monitor;
    }

    public void setConnect_monitor(boolean connect_monitor) {
        this.connect_monitor = connect_monitor;
    }

    public void setYellow_alarm_check_time(int yellow_alarm_check_time) {
        this.yellow_alarm_check_time = yellow_alarm_check_time;
    }

    public void setFall_detection_enabled(boolean fall_detection_enabled) {
        this.fall_detection_enabled = fall_detection_enabled;
    }

    public void setFall_threshold(double fall_threshold) {
        this.fall_threshold = fall_threshold;
    }

    public void setFree_fall_time(int free_fall_time) {
        this.free_fall_time = free_fall_time;
    }

    public void setDisable_on_charging(boolean disable_on_charging) {
        this.disable_on_charging = disable_on_charging;
    }
}
