package sseClient;

import factory.Constants;
import factory.RequestExecutor;
import org.springframework.http.HttpMethod;
import org.springframework.http.ResponseEntity;

import java.util.Timer;
import java.util.TimerTask;

public class Connection implements Runnable {
    private String token;
    private final Timer timer;
    private int delay;


    public Connection(String token) {
        this.token = token;
        timer = new Timer();
    }


    private void pingSender() {
        timer.schedule(new TimerTask() {
            @Override
            public void run() {

                String url = Constants.URI + ":" + Constants.PORT + Constants.PING;

                RequestExecutor.doRequest(url, HttpMethod.GET, token, String.class);

            }
        }, 0, delay);
    }

    public void stopSendingPing() {
        timer.cancel();
    }

    @Override
    public void run() {
        String url = Constants.URI + ":" + Constants.PORT + Constants.PING_PERIOD;

        ResponseEntity<PingPeriodResponse> responsePing = RequestExecutor.doRequest(url, HttpMethod.GET, token, PingPeriodResponse.class);

        if (responsePing.getStatusCodeValue() == 200) {
            delay = responsePing.getBody().getValue();
            System.out.println(responsePing.getBody().getValue());
            pingSender();
        }
    }

}
