package sseClient;

import com.fasterxml.jackson.annotation.JsonProperty;

public class EventBody {
    @JsonProperty("event_types")
    private String[] event_types;


    public EventBody(String[] event_types) {
        this.event_types = event_types;
    }

    public String[] getEvent_types() {
        return event_types;
    }

    public void setEvent_types(String[] event_types) {
        this.event_types = event_types;
    }
}
