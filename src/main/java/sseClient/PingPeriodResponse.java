package sseClient;

import com.fasterxml.jackson.annotation.JsonProperty;
import model.Response;

public class PingPeriodResponse extends Response {
    @JsonProperty("value")
    private int value;

    public int getValue() {
        return value;
    }

    public void setValue(int value) {
        this.value = value;
    }
}
