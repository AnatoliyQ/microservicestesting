package sseClient;

import factory.Constants;
import factory.RequestExecutor;
import org.glassfish.jersey.media.sse.EventSource;
import org.springframework.http.HttpMethod;
import util.SSLUtil;
import util.TokenUtil;

import javax.ws.rs.client.Client;
import javax.ws.rs.client.WebTarget;
import java.io.IOException;

public class Simple {

    public static void main(String[] args) throws Exception {

        final String token = TokenUtil.getToken("user");

        Connection connection = new Connection(token);
        connection.run();

        final String url = Constants.URI + ":" + Constants.PORT + Constants.SUBSCRIBE;

        Thread th = new Thread() {
            public void run() {
                final String pongUrl = Constants.URI + ":" + Constants.PORT + Constants.SSEPONG;

                Client client = null;
                try {
                    client = SSLUtil.getTrustfullSSLClient();
                } catch (Exception e) {
                    e.printStackTrace();
                }

                WebTarget target = client.target(url);

                EventSource eventSource = new EventSource(new AuthorizationHeaderWebTarget(target, token));
                eventSource.register(inboundEvent -> {
                    if (inboundEvent.getName().equals("ping")) {
                        RequestExecutor.doRequest(pongUrl, HttpMethod.GET, token, String.class);
                    } else {
                        System.out.println(inboundEvent.getName());
                        try {
                            System.out.println(inboundEvent.getData());
                        } catch (IOException e) {
                            e.printStackTrace();
                        }
                    }

                });
            }

        };

        th.start();

    }
}
