package util;

import factory.Constants;
import factory.RequestExecutor;
import model.autorization.AuthUserData;
import model.autorization.ResponseAuth;
import org.springframework.http.HttpMethod;
import org.springframework.http.ResponseEntity;

public class TokenUtil {

    public static String getToken(String roleString) {
        String token;
        String url = Constants.URI + ":" + Constants.PORT + Constants.SESSIONS;

        AuthUserData user = new AuthUserData();
        user.setId(Constants.USER_ID);
        user.setServer_id(Constants.SERVER_ID);
        user.setPassword(Constants.USERPASSWORD);
        String[] role = {roleString};
        user.setRoles(role);

        ResponseEntity<ResponseAuth> response = RequestExecutor.doRequest(url, HttpMethod.POST, user, ResponseAuth.class);

        token = "Bearer " + response.getBody().getToken();

        return token;
    }

}
