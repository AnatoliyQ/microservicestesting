package amgw.rpc.channel;

import amgw.data.TestData;
import com.fasterxml.jackson.databind.ser.FilterProvider;
import com.fasterxml.jackson.databind.ser.impl.SimpleBeanPropertyFilter;
import com.fasterxml.jackson.databind.ser.impl.SimpleFilterProvider;
import factory.Constants;
import io.qameta.allure.Epic;
import io.qameta.allure.Feature;
import io.qameta.allure.Severity;
import io.qameta.allure.SeverityLevel;
import model.amgw.rpc.KeyValue;
import model.amgw.rpc.RpcBody;
import model.amgw.rpc.channel.ChannelBody;
import model.amgw.rpc.channel.ChannelResponse;
import model.amgw.rpc.server.ServerResponse;
import model.configService.configResponse;
import org.springframework.http.HttpMethod;
import org.springframework.http.ResponseEntity;
import org.testng.Assert;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

@Epic("AMGW")
@Feature("Create channel")
public class CreateChannelTest {
    private String urlAmgw;
    private String tokenInternal;
    private RpcBody body;


    @BeforeClass
    public void setup() {
        urlAmgw = String.format("%s:%s%s", Constants.URI, Constants.PORT, Constants.AMGW);
        String urlConfig = String.format("%s:%s%s", Constants.URI, Constants.PORT, Constants.AMGW_TOKEN);
        String tokenAdmin = util.TokenUtil.getToken("admin");
        ResponseEntity<configResponse> response = factory.RequestExecutor.doRequest(urlConfig, HttpMethod.GET, tokenAdmin, configResponse.class);
        tokenInternal = "internal " + response.getBody().getValue();
        body = createBody();

    }

    @Test(description = "Create channel with correct settings")
    @Severity(SeverityLevel.BLOCKER)
    public void createChannel() {
        String[] ignorableFieldNames = {"id"};

        FilterProvider filters = new SimpleFilterProvider().addFilter("FilterChannelBody", SimpleBeanPropertyFilter.serializeAllExcept(ignorableFieldNames));

        ResponseEntity<ChannelResponse> response = factory.RequestExecutor.doFilteredRequest(urlAmgw, HttpMethod.POST, tokenInternal, body, filters, ChannelResponse.class);

        Assert.assertEquals(response.getStatusCodeValue(), 200, "The status code is incorrect - ");
        ChannelBody channelBody = response.getBody().getResult();

        Assert.assertEquals(channelBody.getName(), "Test channel", "Channel name is incorrect - ");

        Assert.assertEquals(channelBody.getServer_id(), TestData.server_id, "Wrong server ID");

        Assert.assertNull(response.getBody().getError(), "Response have error!");

        Assert.assertNotNull(channelBody.getConfig(), "No channel config ");

        TestData.channel_id = channelBody.getId();
        TestData.channel_name = channelBody.getName();

    }

    @Test(description = "Create channel without name")
    public void createChannelWithoutName() {
        String[] ignorableFieldNames = {"id", "name"};

        FilterProvider filters = new SimpleFilterProvider().addFilter("FilterChannelBody", SimpleBeanPropertyFilter.serializeAllExcept(ignorableFieldNames));

        ResponseEntity<ChannelResponse> response = factory.RequestExecutor.doFilteredRequest(urlAmgw, HttpMethod.POST, tokenInternal, body, filters, ChannelResponse.class);

        Assert.assertEquals(response.getStatusCodeValue(), 400, "The status code is incorrect - ");
        Assert.assertNull(response.getBody().getResult(), "Result should be Null");

        Assert.assertEquals(response.getBody().getError(), "FIELD_UNDEFINED", "Wrong error message - ");

    }

    @Test(description = "Create channel without params")
    public void createChannelWithoutParams() {

        RpcBody wrongBody = new RpcBody();
        wrongBody.setMethod(Constants.CREATE_CHANNEL_METHOD);

        ResponseEntity<ChannelResponse> response = factory.RequestExecutor.doRequest(urlAmgw, HttpMethod.POST, tokenInternal, wrongBody, ChannelResponse.class);

        Assert.assertEquals(response.getStatusCodeValue(), 400, "The status code is incorrect - ");
        Assert.assertNull(response.getBody().getResult(), "Result should be Null");

        Assert.assertEquals(response.getBody().getError(), "jsonrpc: request body missing params", "Wrong error message - ");

    }

    @Test(description = "Create channel with empty params")
    public void createChannelWithEmptyParams() {

        RpcBody wrongBody = new RpcBody();
        wrongBody.setMethod(Constants.CREATE_CHANNEL_METHOD);
        wrongBody.setParams(new Object[0]);

        ResponseEntity<ServerResponse> response = factory.RequestExecutor.doRequest(urlAmgw, HttpMethod.POST, tokenInternal, wrongBody, ServerResponse.class);

        Assert.assertEquals(response.getStatusCodeValue(), 400, "The status code is incorrect - ");
        Assert.assertNull(response.getBody().getResult(), "Result should be Null");

        Assert.assertEquals(response.getBody().getError(), "SERVER_ID_UNDEFINED", "Wrong error message - ");

    }


    private RpcBody createBody() {
        ChannelBody channelBody = new ChannelBody();
        channelBody.setServer_id(TestData.server_id);
        channelBody.setName("Test channel");

        KeyValue[] channelConfig = new KeyValue[3];
        channelConfig[0] = new KeyValue("select_channel_to_ptt", "true");
        channelConfig[1] = new KeyValue("select_channel_to_ptt_timeout", "10");
        channelConfig[2] = new KeyValue("no_ptt_queue", "true");

        channelBody.setConfig(channelConfig);

        RpcBody body = new RpcBody();
        Object[] params = new Object[1];
        params[0] = channelBody;
        body.setMethod(Constants.CREATE_CHANNEL_METHOD);
        body.setParams(params);

        return body;
    }


}
