package amgw.rpc.channel;

import amgw.data.TestData;
import factory.Constants;
import io.qameta.allure.Epic;
import io.qameta.allure.Feature;
import io.qameta.allure.Severity;
import io.qameta.allure.SeverityLevel;
import model.amgw.rpc.RpcBody;
import model.amgw.rpc.channel.ChannelBody;
import model.amgw.rpc.channel.ChannelResponse;
import model.amgw.rpc.channel.TwoIDRequest;
import org.springframework.http.HttpMethod;
import org.springframework.http.ResponseEntity;
import org.testng.Assert;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

@Epic("AMGW")
@Feature("Read channel")
public class ReadChannelTest {

    private String urlAmgw;
    private String tokenInternal;
    private RpcBody body;

    @BeforeClass
    public void setup() {
        urlAmgw = String.format("%s:%s%s", Constants.URI, Constants.PORT, Constants.AMGW);
        tokenInternal = TestData.amgwServiceToken;
        body = createBody();
    }

    @Test(description = "Read created channel")
    @Severity(SeverityLevel.BLOCKER)
    public void readCreatedChannel() {
        ResponseEntity<ChannelResponse> response = factory.RequestExecutor.doRequest(urlAmgw, HttpMethod.POST, tokenInternal, body, ChannelResponse.class);
        Assert.assertEquals(response.getStatusCodeValue(), 200, "The status code is incorrect - ");

        ChannelBody body = response.getBody().getResult();

        Assert.assertEquals("Test channel", body.getName(), "Channel name is incorrect - ");
        Assert.assertEquals(TestData.server_id, body.getServer_id(), "Wrong server ID ");
        Assert.assertEquals(TestData.channel_id, body.getId(), "Wrong channel ID ");
        Assert.assertNotNull(body.getConfig(), "Config null ");

    }


    @Test(description = "Read channel with empty params")
    public void readChannelWithEmptyParams() {
        RpcBody bodyWithEmptyParams = createBody();
        bodyWithEmptyParams.setParams(new Object[0]);

        ResponseEntity<ChannelResponse> response = factory.RequestExecutor.doRequest(urlAmgw, HttpMethod.POST, tokenInternal, bodyWithEmptyParams, ChannelResponse.class);

        Assert.assertEquals(response.getStatusCodeValue(), 400, "The status code is incorrect - ");
        Assert.assertNull(response.getBody().getResult(), "Result should be null ");
        Assert.assertEquals(response.getBody().getError(), "SERVER_ID_UNDEFINED", "Wrong error message ");

    }

    @Test(description = "Read channel without params")
    public void readChannelWithoutParams() {
        RpcBody wrongBody = new RpcBody();
        wrongBody.setMethod(Constants.READ_CHANNEL_METHOD);

        ResponseEntity<ChannelResponse> response = factory.RequestExecutor.doRequest(urlAmgw, HttpMethod.POST, tokenInternal, wrongBody, ChannelResponse.class);

        Assert.assertEquals(response.getStatusCodeValue(), 400, "The status code is incorrect - ");
        Assert.assertNull(response.getBody().getResult(), "Result should be null ");
        Assert.assertEquals(response.getBody().getError(), "jsonrpc: request body missing params", "Wrong error message ");

    }


    private RpcBody createBody() {
        TwoIDRequest idRequest = new TwoIDRequest();
        idRequest.setId(TestData.channel_id);
        idRequest.setServerId(TestData.server_id);


        RpcBody body = new RpcBody();
        Object[] params = new Object[1];
        params[0] = idRequest;
        body.setMethod(Constants.READ_CHANNEL_METHOD);
        body.setParams(params);

        return body;
    }


}
