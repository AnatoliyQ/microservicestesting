package amgw.rpc.channel;

import amgw.data.TestData;
import com.fasterxml.jackson.databind.ser.FilterProvider;
import com.fasterxml.jackson.databind.ser.impl.SimpleBeanPropertyFilter;
import com.fasterxml.jackson.databind.ser.impl.SimpleFilterProvider;
import factory.Constants;
import io.qameta.allure.Epic;
import io.qameta.allure.Feature;
import io.qameta.allure.Severity;
import io.qameta.allure.SeverityLevel;
import model.amgw.rpc.KeyValue;
import model.amgw.rpc.RpcBody;
import model.amgw.rpc.channel.ChannelBody;
import model.amgw.rpc.channel.ChannelResponse;
import org.springframework.http.HttpMethod;
import org.springframework.http.ResponseEntity;
import org.testng.Assert;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

@Epic("AMGW")
@Feature("Update channel")
public class UpdateChannelTest {

    private String urlAmgw;
    private String tokenInternal;
    private RpcBody body;


    @BeforeClass
    public void setup() {
        urlAmgw = String.format("%s:%s%s", Constants.URI, Constants.PORT, Constants.AMGW);
        tokenInternal = TestData.amgwServiceToken;
        body = createBody();
    }


    @Test(description = "Update Channel name")
    @Severity(SeverityLevel.BLOCKER)
    public void updateChannelName() {
        String[] ignorableFieldNames = {""};

        FilterProvider filters = new SimpleFilterProvider().addFilter("FilterChannelBody", SimpleBeanPropertyFilter.serializeAllExcept(ignorableFieldNames));

        ResponseEntity<ChannelResponse> response = factory.RequestExecutor.doFilteredRequest(urlAmgw, HttpMethod.POST, tokenInternal, body, filters, ChannelResponse.class);

        Assert.assertEquals(response.getStatusCodeValue(), 200, "The status code is incorrect - ");

        ChannelBody body = response.getBody().getResult();

        Assert.assertEquals(body.getName(), "Test channel updated", "Server name is incorrect - ");
        Assert.assertEquals(body.getServer_id(), TestData.server_id, "Wrong number of port ");
        Assert.assertEquals(body.getId(), TestData.channel_id, "Wrong channel ID");
        Assert.assertNotNull(body.getConfig(), "Config should be not Null ");

    }


    @Test(description = "Update channel with empty params")
    public void updateChannelWithEmptyParams() {
        RpcBody bodyWithEmptyParams = createBody();
        bodyWithEmptyParams.setParams(new Object[0]);

        ResponseEntity<ChannelResponse> response = factory.RequestExecutor.doRequest(urlAmgw, HttpMethod.POST, tokenInternal, bodyWithEmptyParams, ChannelResponse.class);

        Assert.assertEquals(response.getStatusCodeValue(), 400, "The status code is incorrect - ");
        Assert.assertNull(response.getBody().getResult(), "Result should be null ");
        Assert.assertEquals(response.getBody().getError(), "SERVER_ID_UNDEFINED", "Wrong error message ");
    }

    @Test(description = "Update channel without params")
    public void updateChannelWithoutParams() {
        RpcBody wrongBody = new RpcBody();
        wrongBody.setMethod(Constants.UPDATE_SERVER_METHOD);

        ResponseEntity<ChannelResponse> response = factory.RequestExecutor.doRequest(urlAmgw, HttpMethod.POST, tokenInternal, wrongBody, ChannelResponse.class);

        Assert.assertEquals(response.getStatusCodeValue(), 400, "The status code is incorrect - ");
        Assert.assertNull(response.getBody().getResult(), "Result should be null ");
        Assert.assertEquals(response.getBody().getError(), "jsonrpc: request body missing params", "Wrong error message ");

    }


    private RpcBody createBody() {
        ChannelBody channelBody = new ChannelBody();
        channelBody.setServer_id(TestData.server_id);
        channelBody.setId(TestData.channel_id);
        channelBody.setName("Test channel updated");

        KeyValue[] channelConfig = new KeyValue[3];
        channelConfig[0] = new KeyValue("select_channel_to_ptt", "false");
        channelConfig[1] = new KeyValue("select_channel_to_ptt_timeout", "5");
        channelConfig[2] = new KeyValue("no_ptt_queue", "false");

        channelBody.setConfig(channelConfig);

        RpcBody body = new RpcBody();
        Object[] params = new Object[1];
        params[0] = channelBody;
        body.setMethod(Constants.UPDATE_CHANNEL_METHOD);
        body.setParams(params);

        return body;
    }
}
