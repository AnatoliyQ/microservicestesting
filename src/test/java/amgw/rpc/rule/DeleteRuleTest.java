package amgw.rpc.rule;

import amgw.data.TestData;
import com.fasterxml.jackson.databind.ser.FilterProvider;
import com.fasterxml.jackson.databind.ser.impl.SimpleBeanPropertyFilter;
import com.fasterxml.jackson.databind.ser.impl.SimpleFilterProvider;
import factory.Constants;
import io.qameta.allure.Epic;
import io.qameta.allure.Feature;
import io.qameta.allure.Severity;
import io.qameta.allure.SeverityLevel;
import model.amgw.rpc.ObjectRpcResponse;
import model.amgw.rpc.RpcBody;
import model.amgw.rpc.rule.RuleBody;
import org.springframework.http.HttpMethod;
import org.springframework.http.ResponseEntity;
import org.testng.Assert;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import java.util.HashMap;

@Epic("AMGW")
@Feature("Delete rule")
public class DeleteRuleTest {
    private String urlAmgw;
    private String tokenInternal;
    private RpcBody body;


    @BeforeClass
    public void setup() {
        urlAmgw = String.format("%s:%s%s", Constants.URI, Constants.PORT, Constants.AMGW);
        tokenInternal = TestData.amgwServiceToken;
        body = createBody();
    }

    @Test(description = "Delete rule")
    @Severity(SeverityLevel.BLOCKER)
    public void deleteRule() {

        String[] ignorableFieldNames = {"team_name", "rule_apply", "rule_deny"};

        FilterProvider filters = new SimpleFilterProvider().addFilter("FilterRuleBody", SimpleBeanPropertyFilter.serializeAllExcept(ignorableFieldNames));

        ResponseEntity<ObjectRpcResponse> response = factory.RequestExecutor.doFilteredRequest(urlAmgw, HttpMethod.POST, tokenInternal, body, filters, ObjectRpcResponse.class);

        Assert.assertEquals(response.getStatusCodeValue(), 200, "The status code is incorrect - ");

        HashMap<String, Boolean> responseMap;

        responseMap = (HashMap) response.getBody().getResult();

        Assert.assertEquals("status", responseMap.keySet().iterator().next(), "Wrong result key - ");

        Assert.assertTrue(responseMap.get("status"), "Result status should be true ");
    }

    private RpcBody createBody() {
        RuleBody ruleBody = new RuleBody();
        ruleBody.setChannel_id(TestData.channel_id);
        ruleBody.setPriority(10);
        ruleBody.setServer_id(TestData.server_id);

        RpcBody body = new RpcBody();
        Object[] params = new Object[1];
        params[0] = ruleBody;
        body.setMethod(Constants.DELETE_RULE_METHOD);
        body.setParams(params);

        return body;
    }
}
