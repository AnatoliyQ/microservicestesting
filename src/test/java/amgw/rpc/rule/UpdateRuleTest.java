package amgw.rpc.rule;

import amgw.data.TestData;
import com.fasterxml.jackson.databind.ser.FilterProvider;
import com.fasterxml.jackson.databind.ser.impl.SimpleBeanPropertyFilter;
import com.fasterxml.jackson.databind.ser.impl.SimpleFilterProvider;
import factory.Constants;
import io.qameta.allure.Epic;
import io.qameta.allure.Feature;
import io.qameta.allure.Severity;
import io.qameta.allure.SeverityLevel;
import model.amgw.rpc.RpcBody;
import model.amgw.rpc.rule.RuleBody;
import model.amgw.rpc.rule.RuleResponse;
import org.springframework.http.HttpMethod;
import org.springframework.http.ResponseEntity;
import org.testng.Assert;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

@Epic("AMGW")
@Feature("Update rule")
public class UpdateRuleTest {
    private String urlAmgw;
    private String tokenInternal;
    private RpcBody body;


    @BeforeClass
    public void setup() {
        urlAmgw = String.format("%s:%s%s", Constants.URI, Constants.PORT, Constants.AMGW);
        tokenInternal = TestData.amgwServiceToken;
        body = createBody();
    }

    @Test(description = "Update rule")
    @Severity(SeverityLevel.BLOCKER)
    public void updateRule() {

        String[] ignorableFieldNames = {""};

        FilterProvider filters = new SimpleFilterProvider().addFilter("FilterRuleBody", SimpleBeanPropertyFilter.serializeAllExcept(ignorableFieldNames));

        ResponseEntity<RuleResponse> response = factory.RequestExecutor.doFilteredRequest(urlAmgw, HttpMethod.POST, tokenInternal, body, filters, RuleResponse.class);

        Assert.assertEquals(response.getStatusCodeValue(), 200, "The status code is incorrect - ");
        RuleBody ruleBody = response.getBody().getResult();

        Assert.assertEquals(ruleBody.getTeam_name(), "Test team", "Wrong team name");

        Assert.assertEquals(ruleBody.getServer_id(), TestData.server_id, "Wrong server ID ");

        Assert.assertEquals(ruleBody.getChannel_id(), TestData.channel_id, "Wrong channel ID");

        Assert.assertEquals(ruleBody.getPriority(), 10, "Wrong priority");

        Assert.assertNull(response.getBody().getError(), "Response have error!");

        String[] ruleApply = ruleBody.getRule_apply();
        String[] ruleDeny = ruleBody.getRule_deny();

        Assert.assertEquals(ruleApply[0], "mute", "Wrong rule apply [0]");
        Assert.assertEquals(ruleApply[1], "move", "Wrong rule apply [1]");

        Assert.assertEquals(ruleDeny[0], "speak", "Wrong rule deny [0]");
        Assert.assertEquals(ruleDeny[1], "enter", "Wrong rule deny [1]");

    }


    private RpcBody createBody() {
        RuleBody ruleBody = new RuleBody();
        ruleBody.setChannel_id(TestData.channel_id);
        ruleBody.setPriority(10);
        ruleBody.setServer_id(TestData.server_id);

        ruleBody.setTeam_name("Test team");
        ruleBody.setRule_apply(new String[]{"move", "mute"});
        ruleBody.setRule_deny(new String[]{"enter", "speak"});

        RpcBody body = new RpcBody();
        Object[] params = new Object[1];
        params[0] = ruleBody;
        body.setMethod(Constants.UPDATE_RULE_METHOD);
        body.setParams(params);

        return body;
    }
}
