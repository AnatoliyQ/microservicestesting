package amgw.rpc.server;

import amgw.data.TestData;
import com.fasterxml.jackson.databind.ser.FilterProvider;
import com.fasterxml.jackson.databind.ser.impl.SimpleBeanPropertyFilter;
import com.fasterxml.jackson.databind.ser.impl.SimpleFilterProvider;
import factory.Constants;
import io.qameta.allure.*;
import model.amgw.rpc.KeyValue;
import model.amgw.rpc.RpcBody;
import model.amgw.rpc.server.ServerBody;
import model.amgw.rpc.server.ServerResponse;
import model.configService.configResponse;
import org.springframework.http.HttpMethod;
import org.springframework.http.ResponseEntity;
import org.testng.Assert;
import org.testng.annotations.BeforeSuite;
import org.testng.annotations.Test;

@Epic("AMGW")
@Feature("Create server")
public class CreateServerTest {
    private String urlAmgw;
    private String tokenInternal;
    private RpcBody body;


    @BeforeSuite
    public void setup() {
        urlAmgw = String.format("%s:%s%s", Constants.URI, Constants.PORT, Constants.AMGW);
        String urlConfig = String.format("%s:%s%s", Constants.URI, Constants.PORT, Constants.AMGW_TOKEN);
        String tokenAdmin = util.TokenUtil.getToken("admin");
        ResponseEntity<configResponse> response = factory.RequestExecutor.doRequest(urlConfig, HttpMethod.GET, tokenAdmin, configResponse.class);
        TestData.amgwServiceToken = "internal " + response.getBody().getValue();
        tokenInternal = TestData.amgwServiceToken;
        body = createbody();
    }

    @Test(description = "Create server with correct settings")
    @Severity(SeverityLevel.BLOCKER)
    @Description("Create server description")
    public void createServer() {
        String[] ignorableFieldNames = {"id", "port", "running", "online_users"};

        FilterProvider filters = new SimpleFilterProvider().addFilter("FilterServerBody", SimpleBeanPropertyFilter.serializeAllExcept(ignorableFieldNames));

        ResponseEntity<ServerResponse> response = factory.RequestExecutor.doFilteredRequest(urlAmgw, HttpMethod.POST, tokenInternal, body, filters, ServerResponse.class);

        Assert.assertEquals(response.getStatusCodeValue(), 200, "The status code is incorrect - ");
        ServerBody serverBody = response.getBody().getResult();

        Assert.assertEquals("Automation testing server", serverBody.getName_alias(), "Server name is incorrect - ");
        Assert.assertFalse(serverBody.isRunning(), "Server should be not running! ");

        Assert.assertEquals(serverBody.getOnline_users(), 0, "Should not be online users! ");

        int expectedPort = 65000 + (serverBody.getId() - 1);

        Assert.assertEquals(serverBody.getPort(), expectedPort, "Wrong number of port !");

        Assert.assertNull(response.getBody().getError(), "Response have error!");

        TestData.name_alias = serverBody.getName_alias();
        TestData.server_id = serverBody.getId();
        TestData.running = serverBody.isRunning();
        TestData.server_port = serverBody.getPort();

    }

    @Test(description = "Create server without name")
    public void createServerWithoutName() {
        String[] ignorableFieldNames = {"id", "port", "running", "online_users", "name_alias"};

        body.getParams();

        FilterProvider filters = new SimpleFilterProvider().addFilter("FilterServerBody", SimpleBeanPropertyFilter.serializeAllExcept(ignorableFieldNames));

        ResponseEntity<ServerResponse> response = factory.RequestExecutor.doFilteredRequest(urlAmgw, HttpMethod.POST, tokenInternal, body, filters, ServerResponse.class);

        Assert.assertEquals(response.getStatusCodeValue(), 400, "The status code is incorrect - ");
        Assert.assertNull(response.getBody().getResult(), "Result should be Null");

        Assert.assertEquals(response.getBody().getError(), "FIELD_UNDEFINED", "Wrong error message - ");

    }

    @Test(description = "Create server without params")
    public void createServerWithoutParams() {

        RpcBody wrongBody = new RpcBody();
        wrongBody.setMethod(Constants.CREATE_SERVER_METHOD);


        ResponseEntity<ServerResponse> response = factory.RequestExecutor.doRequest(urlAmgw, HttpMethod.POST, tokenInternal, wrongBody, ServerResponse.class);

        Assert.assertEquals(response.getStatusCodeValue(), 400, "The status code is incorrect - ");
        Assert.assertNull(response.getBody().getResult(), "Result should be Null");

        Assert.assertEquals(response.getBody().getError(), "jsonrpc: request body missing params", "Wrong error message - ");

    }

    @Test(description = "Create server with empty params")
    public void createServerWithEmptyParams() {

        RpcBody wrongBody = new RpcBody();
        wrongBody.setMethod(Constants.CREATE_SERVER_METHOD);
        wrongBody.setParams(new Object[0]);

        ResponseEntity<ServerResponse> response = factory.RequestExecutor.doRequest(urlAmgw, HttpMethod.POST, tokenInternal, wrongBody, ServerResponse.class);

        Assert.assertEquals(response.getStatusCodeValue(), 400, "The status code is incorrect - ");
        Assert.assertNull(response.getBody().getResult(), "Result should be Null");

        Assert.assertEquals(response.getBody().getError(), "FIELD_UNDEFINED", "Wrong error message - ");

    }


    private RpcBody createbody() {
        ServerBody sbody = new ServerBody();
        sbody.setName_alias("Automation testing server");
        KeyValue[] serverConfig = new KeyValue[12];
        serverConfig[0] = new KeyValue("allow_maintenance", "1");
        serverConfig[1] = new KeyValue("allow_cad", "1");
        serverConfig[2] = new KeyValue("max_groups", "3");
        serverConfig[3] = new KeyValue("transparent_ptt", "1");
        serverConfig[4] = new KeyValue("mt_uri", "https://go.tassta.com:3012");
        serverConfig[5] = new KeyValue("allow_map", "1");
        serverConfig[6] = new KeyValue("allow_priority", "1");
        serverConfig[7] = new KeyValue("allow_history_replay", "1");
        serverConfig[8] = new KeyValue("emc_project", "testproject");
        serverConfig[9] = new KeyValue("external_system_offset", "10000");
        serverConfig[10] = new KeyValue("force_tcp", "0");
        serverConfig[11] = new KeyValue("jb1", "0");
        sbody.setConfig(serverConfig);

        RpcBody body = new RpcBody();
        Object[] params = new Object[1];
        params[0] = sbody;
        body.setMethod(Constants.CREATE_SERVER_METHOD);
        body.setParams(params);

        return body;
    }


}
