package amgw.rpc.server;

import amgw.data.TestData;
import factory.Constants;
import io.qameta.allure.Epic;
import io.qameta.allure.Feature;
import io.qameta.allure.Severity;
import io.qameta.allure.SeverityLevel;
import model.amgw.rpc.IDRequest;
import model.amgw.rpc.RpcBody;
import model.amgw.rpc.server.ServerBody;
import model.amgw.rpc.server.ServerResponse;
import org.springframework.http.HttpMethod;
import org.springframework.http.ResponseEntity;
import org.testng.Assert;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

@Epic("AMGW")
@Feature("Stop server")
public class StopServerTest {
    private String urlAmgw;
    private String tokenInternal;
    private RpcBody body;


    @BeforeClass
    public void setup() {
        urlAmgw = String.format("%s:%s%s", Constants.URI, Constants.PORT, Constants.AMGW);
        tokenInternal = TestData.amgwServiceToken;
        body = createBody();
    }

    @Test(description = "Stop server")
    @Severity(SeverityLevel.BLOCKER)
    public void stopServerTest() {
        ResponseEntity<ServerResponse> response = factory.RequestExecutor.doRequest(urlAmgw, HttpMethod.POST, tokenInternal, body, ServerResponse.class);
        Assert.assertEquals(response.getStatusCodeValue(), 200, "The status code is incorrect - ");

        ServerBody serverBody = response.getBody().getResult();

        Assert.assertEquals(serverBody.getName_alias(), "Updated Automation testing server", "Server name is incorrect - ");

        Assert.assertFalse(serverBody.isRunning(), "Server was not stopped!");

        Assert.assertEquals(serverBody.getOnline_users(), 0, "Should not be online users! ");

        Assert.assertEquals(serverBody.getPort(), TestData.server_port, "Wrong number of port !");
        Assert.assertEquals(serverBody.getId(), TestData.server_id, "Wrong server id !");

        Assert.assertNull(response.getBody().getError(), "Response have error!");

        Assert.assertNotNull(serverBody.getConfig(), "Server config should be not Null !");
    }

    @Test(description = "Stop server with empty params")
    public void stopServerWithEmptyParams() {
        RpcBody bodyWithEmptyParams = createBody();
        bodyWithEmptyParams.setParams(new Object[0]);

        ResponseEntity<ServerResponse> response = factory.RequestExecutor.doRequest(urlAmgw, HttpMethod.POST, tokenInternal, bodyWithEmptyParams, ServerResponse.class);

        Assert.assertEquals(response.getStatusCodeValue(), 400, "The status code is incorrect - ");
        Assert.assertNull(response.getBody().getResult(), "Result should be null ");
        Assert.assertEquals(response.getBody().getError(), "ID_UNDEFINED", "Wrong error message ");

    }

    @Test(description = "Stop server without params")
    public void stopServerWithoutParams() {
        RpcBody wrongBody = new RpcBody();
        wrongBody.setMethod(Constants.START_SERVER_METHOD);

        ResponseEntity<ServerResponse> response = factory.RequestExecutor.doRequest(urlAmgw, HttpMethod.POST, tokenInternal, wrongBody, ServerResponse.class);

        Assert.assertEquals(response.getStatusCodeValue(), 400, "The status code is incorrect - ");
        Assert.assertNull(response.getBody().getResult(), "Result should be null ");
        Assert.assertEquals(response.getBody().getError(), "jsonrpc: request body missing params", "Wrong error message ");

    }

    private RpcBody createBody() {
        IDRequest idRequest = new IDRequest();
        idRequest.setId(TestData.server_id);

        RpcBody body = new RpcBody();
        Object[] params = new Object[1];
        params[0] = idRequest;
        body.setMethod(Constants.STOP_SERVER_METHOD);
        body.setParams(params);

        return body;
    }

}
