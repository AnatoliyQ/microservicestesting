package amgw.rpc.server;

import amgw.data.TestData;
import com.fasterxml.jackson.databind.ser.FilterProvider;
import com.fasterxml.jackson.databind.ser.impl.SimpleBeanPropertyFilter;
import com.fasterxml.jackson.databind.ser.impl.SimpleFilterProvider;
import factory.Constants;
import io.qameta.allure.Epic;
import io.qameta.allure.Feature;
import io.qameta.allure.Severity;
import io.qameta.allure.SeverityLevel;
import model.amgw.rpc.KeyValue;
import model.amgw.rpc.RpcBody;
import model.amgw.rpc.server.ServerBody;
import model.amgw.rpc.server.ServerResponse;
import org.springframework.http.HttpMethod;
import org.springframework.http.ResponseEntity;
import org.testng.Assert;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

@Epic("AMGW")
@Feature("Update server")
public class UpdateServerTest {
    private String urlAmgw;
    private String tokenInternal;
    private RpcBody body;


    @BeforeClass
    public void setup() {
        urlAmgw = String.format("%s:%s%s", Constants.URI, Constants.PORT, Constants.AMGW);
        tokenInternal = TestData.amgwServiceToken;
        body = createBody();
    }


    @Test(description = "Update server name")
    @Severity(SeverityLevel.BLOCKER)
    public void updateServerName() {
        String[] ignorableFieldNames = {"running"};

        FilterProvider filters = new SimpleFilterProvider().addFilter("FilterServerBody", SimpleBeanPropertyFilter.serializeAllExcept(ignorableFieldNames));

        ResponseEntity<ServerResponse> response = factory.RequestExecutor.doFilteredRequest(urlAmgw, HttpMethod.POST, tokenInternal, body, filters, ServerResponse.class);

        Assert.assertEquals(response.getStatusCodeValue(), 200, "The status code is incorrect - ");
        ServerBody serverBody = response.getBody().getResult();

        Assert.assertEquals(serverBody.getName_alias(), "Updated Automation testing server", "Server name is incorrect - ");
        Assert.assertFalse(serverBody.isRunning(), "Server should be not running! ");

        Assert.assertEquals(serverBody.getOnline_users(), 0, "Should not be online users! ");

        Assert.assertEquals(serverBody.getPort(), TestData.server_port, "Wrong number of port !");

        Assert.assertNull(response.getBody().getError(), "Response have error!");

        Assert.assertEquals(TestData.server_id, serverBody.getId(), "Wrong server ID ");

    }

    @Test(description = "Update running field")
    public void updateServerRunningField() {
        String[] ignorableFieldNames = {};

        FilterProvider filters = new SimpleFilterProvider().addFilter("FilterServerBody", SimpleBeanPropertyFilter.serializeAllExcept(ignorableFieldNames));

        ServerBody sbody = (ServerBody) body.getParams()[0];
        sbody.setRunning(true);
        Object[] params = new Object[1];
        params[0] = sbody;
        body.setParams(params);

        ResponseEntity<ServerResponse> response = factory.RequestExecutor.doFilteredRequest(urlAmgw, HttpMethod.POST, tokenInternal, body, filters, ServerResponse.class);

        Assert.assertEquals(response.getStatusCodeValue(), 400, "The status code is incorrect - ");
        Assert.assertNull(response.getBody().getResult(), "Result should be null ");
        Assert.assertEquals(response.getBody().getError(), "RUNNING_DEFINED", "Wrong error message ");

    }

    @Test(description = "Update without ID")
    public void updateServerWithoutID() {
        String[] ignorableFieldNames = {"id", "running"};

        FilterProvider filters = new SimpleFilterProvider().addFilter("FilterServerBody", SimpleBeanPropertyFilter.serializeAllExcept(ignorableFieldNames));

        ServerBody sbody = (ServerBody) body.getParams()[0];
        sbody.setRunning(true);
        Object[] params = new Object[1];
        params[0] = sbody;
        body.setParams(params);

        ResponseEntity<ServerResponse> response = factory.RequestExecutor.doFilteredRequest(urlAmgw, HttpMethod.POST, tokenInternal, body, filters, ServerResponse.class);

        Assert.assertEquals(response.getStatusCodeValue(), 400, "The status code is incorrect - ");
        Assert.assertNull(response.getBody().getResult(), "Result should be null ");
        Assert.assertEquals(response.getBody().getError(), "ID_UNDEFINED", "Wrong error message ");
    }

    @Test(description = "Update server with empty params")
    public void readServerWithEmptyParams() {
        RpcBody bodyWithEmptyParams = createBody();
        bodyWithEmptyParams.setParams(new Object[0]);

        ResponseEntity<ServerResponse> response = factory.RequestExecutor.doRequest(urlAmgw, HttpMethod.POST, tokenInternal, bodyWithEmptyParams, ServerResponse.class);

        Assert.assertEquals(response.getStatusCodeValue(), 400, "The status code is incorrect - ");
        Assert.assertNull(response.getBody().getResult(), "Result should be null ");
        Assert.assertEquals(response.getBody().getError(), "ID_UNDEFINED", "Wrong error message ");
    }

    @Test(description = "Update server without params")
    public void readServerWithoutParams() {
        RpcBody wrongBody = new RpcBody();
        wrongBody.setMethod(Constants.UPDATE_SERVER_METHOD);

        ResponseEntity<ServerResponse> response = factory.RequestExecutor.doRequest(urlAmgw, HttpMethod.POST, tokenInternal, wrongBody, ServerResponse.class);

        Assert.assertEquals(response.getStatusCodeValue(), 400, "The status code is incorrect - ");
        Assert.assertNull(response.getBody().getResult(), "Result should be null ");
        Assert.assertEquals(response.getBody().getError(), "jsonrpc: request body missing params", "Wrong error message ");

    }


    private RpcBody createBody() {
        ServerBody sbody = new ServerBody();
        sbody.setId(TestData.server_id);
        sbody.setPort(TestData.server_port);
        sbody.setName_alias("Updated Automation testing server");
        KeyValue[] serverConfig = new KeyValue[12];
        serverConfig[0] = new KeyValue("allow_maintenance", "1");
        serverConfig[1] = new KeyValue("allow_cad", "1");
        serverConfig[2] = new KeyValue("max_groups", "3");
        serverConfig[3] = new KeyValue("transparent_ptt", "1");
        serverConfig[4] = new KeyValue("mt_uri", "https://go.tassta.com:3012");
        serverConfig[5] = new KeyValue("allow_map", "1");
        serverConfig[6] = new KeyValue("allow_priority", "1");
        serverConfig[7] = new KeyValue("allow_history_replay", "1");
        serverConfig[8] = new KeyValue("emc_project", "testproject");
        serverConfig[9] = new KeyValue("external_system_offset", "10000");
        serverConfig[10] = new KeyValue("force_tcp", "0");
        serverConfig[11] = new KeyValue("jb1", "0");
        sbody.setConfig(serverConfig);

        RpcBody body = new RpcBody();
        Object[] params = new Object[1];
        params[0] = sbody;
        body.setMethod(Constants.UPDATE_SERVER_METHOD);
        body.setParams(params);

        return body;
    }
}
