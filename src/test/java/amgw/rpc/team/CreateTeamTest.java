package amgw.rpc.team;

import amgw.data.TestData;
import com.fasterxml.jackson.databind.ser.FilterProvider;
import com.fasterxml.jackson.databind.ser.impl.SimpleBeanPropertyFilter;
import com.fasterxml.jackson.databind.ser.impl.SimpleFilterProvider;
import factory.Constants;
import io.qameta.allure.Epic;
import io.qameta.allure.Feature;
import io.qameta.allure.Severity;
import io.qameta.allure.SeverityLevel;
import model.amgw.rpc.RpcBody;
import model.amgw.rpc.team.TeamBody;
import model.amgw.rpc.team.TeamResponse;
import org.springframework.http.HttpMethod;
import org.springframework.http.ResponseEntity;
import org.testng.Assert;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

@Epic("AMGW")
@Feature("Create team")
public class CreateTeamTest {
    private String urlAmgw;
    private String tokenInternal;
    private RpcBody body;


    @BeforeClass
    public void setup() {
        urlAmgw = String.format("%s:%s%s", Constants.URI, Constants.PORT, Constants.AMGW);
        tokenInternal = TestData.amgwServiceToken;
        body = createBody();
    }

    @Test(description = "Create team")
    @Severity(SeverityLevel.BLOCKER)
    public void createTeam() {
        String[] ignorableFieldNames = {"id"};

        FilterProvider filters = new SimpleFilterProvider().addFilter("FilterTeamBody", SimpleBeanPropertyFilter.serializeAllExcept(ignorableFieldNames));

        ResponseEntity<TeamResponse> response = factory.RequestExecutor.doFilteredRequest(urlAmgw, HttpMethod.POST, tokenInternal, body, filters, TeamResponse.class);

        Assert.assertEquals(response.getStatusCodeValue(), 200, "The status code is incorrect - ");
        Assert.assertNull(response.getBody().getError(), "Should not be error ");

        TeamBody teamBody = response.getBody().getResult();

        Assert.assertEquals(teamBody.getName(), "test team", "Team name is incorrect - ");
        Assert.assertEquals(teamBody.getServer_id(), TestData.server_id, "Wrong server ID ");
        Assert.assertEquals(teamBody.getUsers()[0], TestData.user_id, "Wrong user id in team ");

        TestData.team_id = teamBody.getId();
    }

    @Test(description = "Create team with existing name")
    public void createTeamWithExistingName() {
        String[] ignorableFieldNames = {"id"};

        FilterProvider filters = new SimpleFilterProvider().addFilter("FilterTeamBody", SimpleBeanPropertyFilter.serializeAllExcept(ignorableFieldNames));

        ResponseEntity<TeamResponse> response = factory.RequestExecutor.doFilteredRequest(urlAmgw, HttpMethod.POST, tokenInternal, body, filters, TeamResponse.class);

        Assert.assertEquals(response.getStatusCodeValue(), 400, "The status code is incorrect - ");
        Assert.assertEquals(response.getBody().getError(), "Failed to save team object", "Wrong error message ");
        Assert.assertNull(response.getBody().getResult(), "Result should be null ");

    }

    @Test(description = "Create team without server id")
    public void createTeamWithoutServerID() {
        String[] ignorableFieldNames = {"id", "server_id"};

        FilterProvider filters = new SimpleFilterProvider().addFilter("FilterTeamBody", SimpleBeanPropertyFilter.serializeAllExcept(ignorableFieldNames));

        ResponseEntity<TeamResponse> response = factory.RequestExecutor.doFilteredRequest(urlAmgw, HttpMethod.POST, tokenInternal, body, filters, TeamResponse.class);

        Assert.assertEquals(response.getStatusCodeValue(), 400, "The status code is incorrect - ");
        Assert.assertEquals(response.getBody().getError(), "SERVER_ID_UNDEFINED", "Wrong error message ");
        Assert.assertNull(response.getBody().getResult(), "Result should be null ");
    }

    @Test(description = "Create team with wrong user")
    public void createTeamWithWrongUser() {
        TeamBody tbody = new TeamBody();
        tbody.setServer_id(TestData.server_id);
        tbody.setName("test wrong team");

        int[] users = {31786};
        tbody.setUsers(users);

        Object[] params = new Object[1];
        params[0] = tbody;
        body.setParams(params);

        String[] ignorableFieldNames = {"id"};

        FilterProvider filters = new SimpleFilterProvider().addFilter("FilterTeamBody", SimpleBeanPropertyFilter.serializeAllExcept(ignorableFieldNames));

        ResponseEntity<TeamResponse> response = factory.RequestExecutor.doFilteredRequest(urlAmgw, HttpMethod.POST, tokenInternal, body, filters, TeamResponse.class);

        Assert.assertEquals(response.getStatusCodeValue(), 400, "The status code is incorrect - ");
        Assert.assertEquals(response.getBody().getError(), "Failed to save team object", "Wrong error message ");
        Assert.assertNull(response.getBody().getResult(), "Result should be null ");
    }

    @Test(description = "Create team with wrong server")
    public void createTeamWithWrongServerID() {
        TeamBody tbody = new TeamBody();
        tbody.setServer_id(1245);
        tbody.setName("test wrong team");

        Object[] params = new Object[1];
        params[0] = tbody;
        body.setParams(params);

        String[] ignorableFieldNames = {"id"};

        FilterProvider filters = new SimpleFilterProvider().addFilter("FilterTeamBody", SimpleBeanPropertyFilter.serializeAllExcept(ignorableFieldNames));

        ResponseEntity<TeamResponse> response = factory.RequestExecutor.doFilteredRequest(urlAmgw, HttpMethod.POST, tokenInternal, body, filters, TeamResponse.class);

        Assert.assertEquals(response.getStatusCodeValue(), 400, "The status code is incorrect - ");
        Assert.assertEquals(response.getBody().getError(), "NOT_FOUND", "Wrong error message ");
        Assert.assertNull(response.getBody().getResult(), "Result should be null ");
    }


    private RpcBody createBody() {
        TeamBody tbody = new TeamBody();
        tbody.setServer_id(TestData.server_id);
        tbody.setName("test team");

        int[] users = {TestData.user_id};
        tbody.setUsers(users);

        RpcBody body = new RpcBody();
        Object[] params = new Object[1];
        params[0] = tbody;
        body.setMethod(Constants.CREATE_TEAM_METHOD);
        body.setParams(params);

        return body;
    }
}
