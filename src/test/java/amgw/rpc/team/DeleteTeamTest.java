package amgw.rpc.team;

import amgw.data.TestData;
import factory.Constants;
import io.qameta.allure.Epic;
import io.qameta.allure.Feature;
import io.qameta.allure.Severity;
import io.qameta.allure.SeverityLevel;
import model.amgw.rpc.ObjectRpcResponse;
import model.amgw.rpc.RpcBody;
import model.amgw.rpc.channel.TwoIDRequest;
import org.springframework.http.HttpMethod;
import org.springframework.http.ResponseEntity;
import org.testng.Assert;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import java.util.HashMap;

@Epic("AMGW")
@Feature("Delete team")
public class DeleteTeamTest {
    private String urlAmgw;
    private String tokenInternal;
    private RpcBody body;

    @BeforeClass
    public void setup() {
        urlAmgw = String.format("%s:%s%s", Constants.URI, Constants.PORT, Constants.AMGW);
        tokenInternal = TestData.amgwServiceToken;
        body = createBody();
    }

    @Test(description = "Delete created team")
    @Severity(SeverityLevel.BLOCKER)
    public void deleteCreatedTeam() {
        ResponseEntity<ObjectRpcResponse> response = factory.RequestExecutor.doRequest(urlAmgw, HttpMethod.POST, tokenInternal, body, ObjectRpcResponse.class);
        Assert.assertEquals(response.getStatusCodeValue(), 200, "The status code is incorrect - ");

        HashMap<String, Boolean> responseMap;

        responseMap = (HashMap) response.getBody().getResult();

        Assert.assertEquals("status", responseMap.keySet().iterator().next(), "Wrong result key - ");

        Assert.assertTrue(responseMap.get("status"), "Result status should be true ");
    }


    private RpcBody createBody() {
        TwoIDRequest idRequest = new TwoIDRequest();
        idRequest.setId(TestData.team_id);
        idRequest.setServerId(TestData.server_id);

        RpcBody body = new RpcBody();
        Object[] params = new Object[1];
        params[0] = idRequest;
        body.setMethod(Constants.DELETE_TEAM_METHOD);
        body.setParams(params);

        return body;
    }
}
