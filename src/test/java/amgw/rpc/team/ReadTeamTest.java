package amgw.rpc.team;

import amgw.data.TestData;
import factory.Constants;
import io.qameta.allure.Epic;
import io.qameta.allure.Feature;
import io.qameta.allure.Severity;
import io.qameta.allure.SeverityLevel;
import model.amgw.rpc.RpcBody;
import model.amgw.rpc.channel.TwoIDRequest;
import model.amgw.rpc.team.TeamBody;
import model.amgw.rpc.team.TeamResponse;
import org.springframework.http.HttpMethod;
import org.springframework.http.ResponseEntity;
import org.testng.Assert;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

@Epic("AMGW")
@Feature("Read team")
public class ReadTeamTest {
    private String urlAmgw;
    private String tokenInternal;
    private RpcBody body;

    @BeforeClass
    public void setup() {
        urlAmgw = String.format("%s:%s%s", Constants.URI, Constants.PORT, Constants.AMGW);
        tokenInternal = TestData.amgwServiceToken;
        body = createBody();
    }

    @Test(description = "Read created team")
    @Severity(SeverityLevel.BLOCKER)
    public void readCreatedTeam() {
        ResponseEntity<TeamResponse> response = factory.RequestExecutor.doRequest(urlAmgw, HttpMethod.POST, tokenInternal, body, TeamResponse.class);
        Assert.assertEquals(response.getStatusCodeValue(), 200, "The status code is incorrect - ");
        Assert.assertNull(response.getBody().getError(), "Should not be error ");

        TeamBody body = response.getBody().getResult();

        Assert.assertEquals(body.getName(), "test team", "Team name is incorrect - ");
        Assert.assertEquals(TestData.server_id, body.getServer_id(), "Wrong server ID ");
        Assert.assertEquals(body.getId(), TestData.team_id, "Wrong team ID ");
        Assert.assertEquals(body.getUsers()[0], TestData.user_id, "Wrong user in team ");

    }

    @Test(description = "Read team with wrong ID")
    public void readTeamWithWrongId() {
        TwoIDRequest idRequest = new TwoIDRequest();
        idRequest.setId(555);
        idRequest.setServerId(TestData.server_id);

        Object[] params = new Object[1];
        params[0] = idRequest;
        body.setParams(params);

        ResponseEntity<TeamResponse> response = factory.RequestExecutor.doRequest(urlAmgw, HttpMethod.POST, tokenInternal, body, TeamResponse.class);
        Assert.assertEquals(response.getStatusCodeValue(), 400, "The status code is incorrect - ");

        Assert.assertEquals(response.getBody().getError(), "Team not found");
        Assert.assertEquals(response.getBody().getId(), 0, "Id should be 0");
        Assert.assertNull(response.getBody().getResult(), "Result should be null");

    }

    private RpcBody createBody() {
        TwoIDRequest idRequest = new TwoIDRequest();
        idRequest.setId(TestData.team_id);
        idRequest.setServerId(TestData.server_id);

        RpcBody body = new RpcBody();
        Object[] params = new Object[1];
        params[0] = idRequest;
        body.setMethod(Constants.READ_TEAM_METHOD);
        body.setParams(params);

        return body;
    }
}
