package amgw.rpc.users;

import amgw.data.TestData;
import com.fasterxml.jackson.databind.ser.FilterProvider;
import com.fasterxml.jackson.databind.ser.impl.SimpleBeanPropertyFilter;
import com.fasterxml.jackson.databind.ser.impl.SimpleFilterProvider;
import factory.Constants;
import io.qameta.allure.Epic;
import io.qameta.allure.Feature;
import io.qameta.allure.Severity;
import io.qameta.allure.SeverityLevel;
import model.amgw.rpc.KeyValue;
import model.amgw.rpc.RpcBody;
import model.amgw.rpc.user.UserBody;
import model.amgw.rpc.user.UserResponse;
import org.springframework.http.HttpMethod;
import org.springframework.http.ResponseEntity;
import org.testng.Assert;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

@Epic("AMGW")
@Feature("Create user")
public class CreateUserTest {
    private String urlAmgw;
    private String tokenInternal;
    private RpcBody body;


    @BeforeClass
    public void setup() {
        urlAmgw = String.format("%s:%s%s", Constants.URI, Constants.PORT, Constants.AMGW);
        tokenInternal = TestData.amgwServiceToken;
        body = createBody();
    }

    @Test(description = "Create user")
    @Severity(SeverityLevel.BLOCKER)
    public void createUser() {
        String[] ignorableFieldNames = {"id"};

        FilterProvider filters = new SimpleFilterProvider().addFilter("FilterUserBody", SimpleBeanPropertyFilter.serializeAllExcept(ignorableFieldNames));

        ResponseEntity<UserResponse> response = factory.RequestExecutor.doFilteredRequest(urlAmgw, HttpMethod.POST, tokenInternal, body, filters, UserResponse.class);

        Assert.assertEquals(response.getStatusCodeValue(), 200, "The status code is incorrect - ");
        Assert.assertNull(response.getBody().getError(), "Should not be error ");
        UserBody userBody = response.getBody().getResult();

        Assert.assertEquals(userBody.getName(), "TestUser", "User name is incorrect - ");
        Assert.assertEquals(userBody.getPassword(), "89e495e7941cf9e40e6980d14a16bf023ccd4c91", "Password is incorrect - ");
        Assert.assertEquals(userBody.getLast_activity(), 1532937971, "Last activity time is incorrect - ");

        TestData.user_id = userBody.getId();
    }

    @Test(description = "Create user with exist name")
    public void createUserWithExistName() {
        String[] ignorableFieldNames = {"id"};

        FilterProvider filters = new SimpleFilterProvider().addFilter("FilterUserBody", SimpleBeanPropertyFilter.serializeAllExcept(ignorableFieldNames));

        ResponseEntity<UserResponse> response = factory.RequestExecutor.doFilteredRequest(urlAmgw, HttpMethod.POST, tokenInternal, body, filters, UserResponse.class);

        Assert.assertEquals(response.getStatusCodeValue(), 400, "The status code is incorrect - ");
        Assert.assertEquals(response.getBody().getError(), "User with such name already exists", "Wrong error message ");
    }

    @Test(description = "Create user without server ID")
    public void createUserWithoutSeverId() {
        String[] ignorableFieldNames = {"id", "server_id"};

        FilterProvider filters = new SimpleFilterProvider().addFilter("FilterUserBody", SimpleBeanPropertyFilter.serializeAllExcept(ignorableFieldNames));

        ResponseEntity<UserResponse> response = factory.RequestExecutor.doFilteredRequest(urlAmgw, HttpMethod.POST, tokenInternal, body, filters, UserResponse.class);

        Assert.assertEquals(response.getStatusCodeValue(), 400, "The status code is incorrect - ");
        Assert.assertEquals(response.getBody().getError(), "SERVER_ID_UNDEFINED", "Wrong error message ");
    }

    @Test(description = "Create user without password")
    public void createUserWithoutPassword() {
        String[] ignorableFieldNames = {"id", "password"};

        FilterProvider filters = new SimpleFilterProvider().addFilter("FilterUserBody", SimpleBeanPropertyFilter.serializeAllExcept(ignorableFieldNames));

        ResponseEntity<UserResponse> response = factory.RequestExecutor.doFilteredRequest(urlAmgw, HttpMethod.POST, tokenInternal, body, filters, UserResponse.class);

        Assert.assertEquals(response.getStatusCodeValue(), 400, "The status code is incorrect - ");
        Assert.assertEquals(response.getBody().getError(), "FIELD_UNDEFINED", "Wrong error message ");
    }


    private RpcBody createBody() {
        UserBody ubody = new UserBody();
        ubody.setServer_id(TestData.server_id);
        ubody.setName("TestUser");
        ubody.setPassword("89e495e7941cf9e40e6980d14a16bf023ccd4c91");
        ubody.setLast_activity(1532937971);

        KeyValue[] userConfig = new KeyValue[64];
        userConfig[0] = new KeyValue("allow_guard_tours", "1");
        userConfig[1] = new KeyValue("allow_logout_with_password", "0");
        userConfig[2] = new KeyValue("name_alias", "3");
        userConfig[3] = new KeyValue("client_version", "");
        userConfig[4] = new KeyValue("device_id", "355090085948323");
        userConfig[5] = new KeyValue("picture_provider", "1");
        userConfig[6] = new KeyValue("ambience_listening", "1");
        userConfig[7] = new KeyValue("allow_ch", "1");
        userConfig[8] = new KeyValue("allow_individual_call", "1");
        userConfig[9] = new KeyValue("allow_emergency_acknowledge", "0");
        userConfig[10] = new KeyValue("bitrate", "10000");
        userConfig[11] = new KeyValue("emergency_warning_time", "5");
        userConfig[12] = new KeyValue("no_ptt_btn", "0");
        userConfig[13] = new KeyValue("abcall", "1");
        userConfig[14] = new KeyValue("authenticate_device_id", "0");
        userConfig[15] = new KeyValue("allow_lone_worker", "1");
        userConfig[16] = new KeyValue("complexity", "-1");
        userConfig[17] = new KeyValue("a_mtlstn", "1");
        userConfig[18] = new KeyValue("connection_alarm", "1");
        userConfig[19] = new KeyValue("priority", "0");
        userConfig[20] = new KeyValue("ae2e", "1");
        userConfig[21] = new KeyValue("alntf", "1");
        userConfig[22] = new KeyValue("allow_mute", "1");
        userConfig[23] = new KeyValue("service_user", "0");
        userConfig[24] = new KeyValue("allow_ih", "1");
        userConfig[25] = new KeyValue("logout_password", "");
        userConfig[26] = new KeyValue("audio_available", "86340");
        userConfig[27] = new KeyValue("allow_sl", "1");
        userConfig[28] = new KeyValue("allow_sdk", "1");
        userConfig[29] = new KeyValue("allow_ep", "0");
        userConfig[30] = new KeyValue("photo_status", "1");
        userConfig[31] = new KeyValue("allow_em", "1");
        userConfig[32] = new KeyValue("a_device_details", "1");
        userConfig[33] = new KeyValue("forbidden_remote_control", "0");
        userConfig[34] = new KeyValue("fec", "1");
        userConfig[35] = new KeyValue("use_alternative_user_id", "0");
        userConfig[36] = new KeyValue("allow_u", "1");
        userConfig[37] = new KeyValue("volume_max_in_alarm", "0");
        userConfig[38] = new KeyValue("allow_map", "1");
        userConfig[39] = new KeyValue("forbidden_to_send_gps", "0");
        userConfig[40] = new KeyValue("allow_wk", "1");
        userConfig[41] = new KeyValue("crisis_team_member", "1");
        userConfig[42] = new KeyValue("allow_lwpl", "1");
        userConfig[43] = new KeyValue("alternative_user_id", "");
        userConfig[44] = new KeyValue("paket_loss_perc", "20");
        userConfig[45] = new KeyValue("allow_tr", "1");
        userConfig[46] = new KeyValue("allow_rv", "1");
        userConfig[47] = new KeyValue("adic", "1");
        userConfig[48] = new KeyValue("allow_msg", "1");
        userConfig[49] = new KeyValue("allow_stngs", "1");
        userConfig[50] = new KeyValue("naemstop", "0");
        userConfig[51] = new KeyValue("amcall", "1");
        userConfig[52] = new KeyValue("allow_h", "1");
        userConfig[53] = new KeyValue("allow_cad", "1");
        userConfig[54] = new KeyValue("ambience_listener", "1");
        userConfig[55] = new KeyValue("t_noact_ic", "0");
        userConfig[56] = new KeyValue("allow_geojson_edit", "1");
        userConfig[57] = new KeyValue("max_list_len", "100");
        userConfig[58] = new KeyValue("vbr", "1");
        userConfig[59] = new KeyValue("group", "0");
        userConfig[60] = new KeyValue("picture_master", "1");
        userConfig[61] = new KeyValue("afeatstat", "1");
        userConfig[62] = new KeyValue("rpntf", "1");
        userConfig[63] = new KeyValue("sl_time", "10");

        ubody.setConfig(userConfig);

        RpcBody body = new RpcBody();
        Object[] params = new Object[1];
        params[0] = ubody;
        body.setMethod(Constants.CREATE_USER_METHOD);
        body.setParams(params);

        return body;
    }

}
