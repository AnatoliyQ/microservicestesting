package amgw.rpc.users;

import amgw.data.TestData;
import factory.Constants;
import io.qameta.allure.Epic;
import io.qameta.allure.Feature;
import io.qameta.allure.Severity;
import io.qameta.allure.SeverityLevel;
import model.amgw.rpc.RpcBody;
import model.amgw.rpc.channel.TwoIDRequest;
import model.amgw.rpc.user.UserBody;
import model.amgw.rpc.user.UserResponse;
import org.springframework.http.HttpMethod;
import org.springframework.http.ResponseEntity;
import org.testng.Assert;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

@Epic("AMGW")
@Feature("Read user")
public class ReadUserTest {
    private String urlAmgw;
    private String tokenInternal;
    private RpcBody body;

    @BeforeClass
    public void setup() {
        urlAmgw = String.format("%s:%s%s", Constants.URI, Constants.PORT, Constants.AMGW);
        tokenInternal = TestData.amgwServiceToken;
        body = createBody();
    }

    @Test(description = "Read created user")
    @Severity(SeverityLevel.BLOCKER)
    public void readUser() {
        ResponseEntity<UserResponse> response = factory.RequestExecutor.doRequest(urlAmgw, HttpMethod.POST, tokenInternal, body, UserResponse.class);
        Assert.assertEquals(response.getStatusCodeValue(), 200, "The status code is incorrect -");

        UserBody userBody = response.getBody().getResult();

        Assert.assertEquals(userBody.getName(), "TestUser", "User name is incorrect - ");

        Assert.assertEquals(userBody.getLast_activity(), 1532937971, "Server should be not running! ");

        Assert.assertEquals(userBody.getPassword(), "89e495e7941cf9e40e6980d14a16bf023ccd4c91", "Wrong user password ");

        Assert.assertEquals(userBody.getServer_id(), TestData.server_id, "Wrong server id !");
        Assert.assertEquals(userBody.getId(), TestData.user_id, "Wrong user id !");

        Assert.assertNull(response.getBody().getError(), "Response have error!");

        Assert.assertNotNull(userBody.getConfig(), "Server config should be not Null !");
    }


    private RpcBody createBody() {
        TwoIDRequest idRequest = new TwoIDRequest();
        idRequest.setId(TestData.user_id);
        idRequest.setServerId(TestData.server_id);

        RpcBody body = new RpcBody();
        Object[] params = new Object[1];
        params[0] = idRequest;
        body.setMethod(Constants.READ_USER_METHOD);
        body.setParams(params);

        return body;
    }
}
