package authorizationService;

import factory.Constants;
import factory.RequestExecutor;
import io.qameta.allure.Epic;
import io.qameta.allure.Severity;
import io.qameta.allure.SeverityLevel;
import model.Response;
import model.autorization.AuthUserData;
import model.autorization.ResponseAuth;
import model.autorization.ResponseSessionList;
import org.springframework.http.HttpMethod;
import org.springframework.http.ResponseEntity;
import org.testng.Assert;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

@Epic("Authorization service")
public class AuthorizationTest {
    private String url;
    private String token;


    @BeforeClass
    public void setup() {
        url = String.format("%s:%s%s", Constants.URI, Constants.PORT, Constants.SESSIONS);
    }


    @Test(description = "Authorization on service with user role")
    @Severity(SeverityLevel.BLOCKER)
    public void authorisationCorrect() {

        AuthUserData user = new AuthUserData();
        user.setId(Constants.USER_ID);
        user.setServer_id(Constants.SERVER_ID);
        user.setPassword(Constants.USERPASSWORD);
        String[] role = {"user"};
        user.setRoles(role);

        ResponseEntity<ResponseAuth> response = RequestExecutor.doRequest(url, HttpMethod.POST, user, ResponseAuth.class);

        token = "Bearer " + response.getBody().getToken();

        Assert.assertEquals(response.getStatusCodeValue(), 201, "The status code is incorrect - ");
        Assert.assertTrue(response.getBody().isSuccess(), "Success false! ");
    }

    @Test(dependsOnMethods = {"authorisationCorrect"}, description = "Check the authorization was correct")
    @Severity(SeverityLevel.NORMAL)
//    @Feature("Authorization on service with user role")
    public void listSession() {
        ResponseEntity<ResponseSessionList> response = RequestExecutor.doRequest(url, HttpMethod.GET, token, ResponseSessionList.class);

        ResponseSessionList body = response.getBody();

        Assert.assertEquals(response.getStatusCodeValue(), 200, "The status code is incorrect - ");
        Assert.assertTrue(body.isSuccess(), "Success false! ");
        Assert.assertEquals(body.getUser().getId(), 70);
        Assert.assertEquals(body.getUser().getRoles()[0], "user", "Wrong role");
        Assert.assertEquals(body.getUser().getServer_id(), 2, "Wrong Server ID");
        Assert.assertEquals(body.getUser().getService_name(), "");
        Assert.assertEquals(body.getMessage(), "Session details retrieved");
    }

    @Test(dependsOnMethods = {"listSession"}, description = "Delete authorization token")
    public void deleteSession() {
        ResponseEntity<Response> response = RequestExecutor.doRequest(url, HttpMethod.DELETE, token, Response.class);

        Response body = response.getBody();

        Assert.assertEquals(response.getStatusCodeValue(), 200, "The status code is incorrect - ");
        Assert.assertEquals(body.getMessage(), "Session removed");
        Assert.assertTrue(body.isSuccess(), "Success false! ");
    }

    @Test(description = "Authorization with wrong user ID")
    public void authorisationWithIncorrectUser() {
        AuthUserData user = new AuthUserData();
        user.setId(100500);
        user.setServer_id(Constants.SERVER_ID);
        user.setPassword(Constants.USERPASSWORD);
        String[] role = {"user"};
        user.setRoles(role);

        ResponseEntity<ResponseAuth> response = RequestExecutor.doRequest(url, HttpMethod.POST, user, ResponseAuth.class);

        Assert.assertEquals(response.getStatusCodeValue(), 400, "The status code is incorrect - ");
        Assert.assertFalse(response.getBody().isSuccess(), "Success false! ");
    }

    @Test(description = "Authorization with wrong password")
    public void authorisationWithWrongPassword() {
        AuthUserData user = new AuthUserData();
        user.setId(Constants.USER_ID);
        user.setServer_id(Constants.SERVER_ID);
        user.setPassword("IThinkThisIsAWrongPassword");
        String[] role = {"user"};
        user.setRoles(role);

        ResponseEntity<ResponseAuth> response = RequestExecutor.doRequest(url, HttpMethod.POST, user, ResponseAuth.class);

        Assert.assertEquals(response.getStatusCodeValue(), 400, "The status code is incorrect - ");
        Assert.assertFalse(response.getBody().isSuccess(), "Success false! ");
    }

    @Test(description = "Authorization with wrong server ID")
    public void authorisationWithWrongServerID() {
        AuthUserData user = new AuthUserData();
        user.setId(Constants.USER_ID);
        user.setServer_id(100500);
        user.setPassword(Constants.USERPASSWORD);
        String[] role = {"user"};
        user.setRoles(role);

        ResponseEntity<ResponseAuth> response = RequestExecutor.doRequest(url, HttpMethod.POST, user, ResponseAuth.class);

        Assert.assertEquals(response.getStatusCodeValue(), 400, "The status code is incorrect - ");
        Assert.assertFalse(response.getBody().isSuccess(), "Success false! ");
    }


}
